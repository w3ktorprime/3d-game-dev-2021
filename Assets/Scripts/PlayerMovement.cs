using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed;
    public Rigidbody playerRigidBody;
    
    // Start is called before the first frame update
    void Start()
    {
        // Initialization
        playerRigidBody = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        /*Debug.Log("H: " + Input.GetAxis("Horizontal"));
        Debug.Log("V: " + Input.GetAxis("Vertical"));*/
        
        var playerMovementVector = new Vector3(
            Input.GetAxis("Horizontal") * playerSpeed, 
            Input.GetAxis("Jump"), 
            Input.GetAxis("Vertical") * playerSpeed);
        
        playerRigidBody.AddForce(playerMovementVector, ForceMode.Impulse);
    }
}
