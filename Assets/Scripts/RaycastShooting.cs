using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShooting : MonoBehaviour
{
    
    [SerializeField] private Transform rayOrigin;
    [SerializeField] private float rayMaxDistance;
    [SerializeField] private LayerMask layerMask;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            if(
                Physics.Raycast(
                    rayOrigin.position, 
                    rayOrigin.transform.forward, 
                    out hit, 
                    rayMaxDistance,
                    layerMask)
                )
            {
                Debug.Log("HIT! Name: " + hit.transform.name);
                float distance = Vector3.Distance(rayOrigin.position, hit.point);
                Debug.Log("Distance: " + distance);
                
                // Damage
                hit.transform.GetComponent<HealthManager>().ModifyHealth(-10);
            }
        }
        
        if (Input.GetMouseButtonDown(2))
        {
            RaycastHit hit;
            if(
                Physics.Raycast(
                    rayOrigin.position, 
                    rayOrigin.transform.forward, 
                    out hit, 
                    rayMaxDistance,
                    layerMask)
            )
            {
                Debug.Log("HIT! Name: " + hit.transform.name);
                float distance = Vector3.Distance(rayOrigin.position, hit.point);
                Debug.Log("Distance: " + distance);
                
                // Damage
                hit.transform.GetComponent<HealthManager>().ModifyHealth(10);
            }
        }
    }
}
