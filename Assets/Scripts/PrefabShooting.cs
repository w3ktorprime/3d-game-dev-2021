using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabShooting : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform projectileOrigin;
    [SerializeField] private float shootingForce;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject bulletClone = Instantiate(bulletPrefab, projectileOrigin.position, projectileOrigin.rotation);
            bulletClone.GetComponent<Rigidbody>().AddForce(Vector3.forward * shootingForce, ForceMode.Impulse);
        }
    }
}
