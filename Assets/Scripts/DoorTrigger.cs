using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    [SerializeField] private Transform hingeTransform;
    [SerializeField] private bool isDoorOpen;
    [SerializeField] private bool isTriggered;
    [SerializeField] private GameObject usePanel;
    [SerializeField] private TextMeshProUGUI useTextMeshPro;
    
    private string originalUseText;

    private void Start()
    {
        // Elmentjük az eredeti formázandó textet (változó helye: {0}, editorban lett beállítva)
        originalUseText = useTextMeshPro.text;
    }

    private void Update()
    {
        // UI beállítások
        if (isTriggered)
        {
            if (isDoorOpen)
            {
                useTextMeshPro.text = String.Format(originalUseText, "close");
            }
            else
            {
                useTextMeshPro.text = String.Format(originalUseText, "open");
            }
        }
        
        // Ha trigger zónában vagyunk és lenyomjuk az "E" billentyűt
        if (isTriggered && Input.GetKeyDown(KeyCode.E))
        {
            isDoorOpen = !isDoorOpen;
        }
        
        // Ha ki akarjuk nyitni az ajtót, de még nem értük el a nyitott állást, akkor tovább nyitjuk
        if (isDoorOpen && hingeTransform.rotation.eulerAngles != new Vector3(0,270,0))
        {
            hingeTransform.rotation = Quaternion.RotateTowards(hingeTransform.rotation, Quaternion.Euler(0,-90,0), Time.deltaTime * 10);
        }
        // Ha be akarjuk csukni az ajtót, de még nem értük el a csukott állást, akkor tovább csukjuk
        else if(!isDoorOpen && hingeTransform.rotation.eulerAngles != new Vector3(0,0,0))
        {
            hingeTransform.rotation = Quaternion.RotateTowards(hingeTransform.rotation, Quaternion.Euler(0,0,0), Time.deltaTime * 10);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        isTriggered = true;
        usePanel.SetActive(isTriggered);
        // isDoorOpen = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isTriggered = false;
        usePanel.SetActive(isTriggered);
        // isDoorOpen = false
    }
}
