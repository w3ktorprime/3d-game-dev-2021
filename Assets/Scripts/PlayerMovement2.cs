using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2 : MonoBehaviour
{
    [SerializeField] private float playerSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var horValue = Input.GetAxis("Horizontal");
        var verValue = Input.GetAxis("Vertical");
        Vector3 movementVector = new Vector3(horValue, 0, verValue);
        movementVector.Normalize();
        movementVector *= playerSpeed;
        
        
        transform.Translate(movementVector, Space.Self);
    }
}
