using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateController : MonoBehaviour
{
    [SerializeField] 
    private Animator animator;
    [SerializeField]
    private bool isWalking;
    [SerializeField]
    private bool isRunning;

    private int isWalkingHash;
    private int isRunningHash;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();

        isWalkingHash = Animator.StringToHash("isWalking");
        isRunningHash = Animator.StringToHash("isRunning");
    }

    // Update is called once per frame
    void Update()
    {
        // Walking
        if (!isWalking && Input.GetKey(KeyCode.W)) // KeyCode.W == "w"
        {
            isWalking = true;
            animator.SetBool(isWalkingHash, isWalking);
        }
        
        if (isWalking && !Input.GetKey(KeyCode.W)) // KeyCode.W == "w"
        {
            isWalking = false;
            animator.SetBool(isWalkingHash, isWalking);
        }
        
        // Running
        if ( !isRunning && (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)) ) // KeyCode.W == "w"
        {
            isRunning = true;
            animator.SetBool(isRunningHash, isRunning);
        }
        
        if ( isRunning && (!Input.GetKey(KeyCode.W) || !Input.GetKey(KeyCode.LeftShift)) ) // KeyCode.W == "w"
        {
            isRunning = false;
            animator.SetBool(isRunningHash, isRunning);
        }
    }
}