using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] private float bulletMaxLifetime;
    [SerializeField] private GameObject particleSystem;
    
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, bulletMaxLifetime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        //Damage
        other.gameObject.GetComponent<HealthManager>()?.ModifyHealth(-20);
        
        Invoke(nameof(DeattachParSys), 0.2f);
        Destroy(gameObject, 0.2f);
    }

    void DeattachParSys()
    {
        particleSystem.transform.SetParent(null);
        particleSystem.transform.localScale = Vector3.one;
        Destroy(particleSystem,5);
    }
}
