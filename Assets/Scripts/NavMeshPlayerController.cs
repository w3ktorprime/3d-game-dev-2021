using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshPlayerController : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agent;
    [SerializeField]
    private Vector3 hitDestination;
    [SerializeField]
    private GameObject hitObject;
    // Start is called before the first frame update
    void Start()
    {
        // agent = gameObject.GetComponent<NavMeshAgent>();
        // cam = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                agent.SetDestination(hit.point);
                hitDestination = hit.point;
                hitObject = hit.collider.gameObject;
            }
        }
    }
}
