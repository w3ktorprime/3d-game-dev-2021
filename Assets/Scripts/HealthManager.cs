using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private float maxHealth;
    [SerializeField] private float currentHealth;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!GetComponent<EnemyController>())
        {
            currentHealth = maxHealth;
            return;
        }
        maxHealth = GetComponent<EnemyController>().healthPoint;
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ModifyHealth(float value)
    {
        currentHealth += value;

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
}
