using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsScript : MonoBehaviour
{
    [SerializeField]
    private Text fpsText;
    private float fpsCount;
    [SerializeField]
    private float rate;
    private float countDown;
    
    // Start is called before the first frame update
    void Start()
    {
        fpsText = GetComponent<Text>();
        countDown = rate;
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown <= 0)
        {
            ShowFps();
            countDown = rate;
        }
    }

    void ShowFps()
    {
        fpsCount = 1 / Time.deltaTime;
        fpsText.text = fpsCount.ToString("F0") + " FPS";
    }
}
