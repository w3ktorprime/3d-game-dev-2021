using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    [SerializeField] private Animator doorAnimator;
    //[SerializeField] private Transform hingeTransform;
    [SerializeField] private bool isDoorOpen;
    [SerializeField] private bool isTriggered;
    [SerializeField] private GameObject usePanel;
    [SerializeField] private TextMeshProUGUI useTextMeshPro;
    
    private string originalUseText;
    private static readonly int IsDoorOpened = Animator.StringToHash("isDoorOpened");
    private bool _isdoorAnimatorNotNull;

    private void Start()
    {
        // Elmentjük az eredeti formázandó textet (változó helye: {0}, editorban lett beállítva)
        originalUseText = useTextMeshPro.text;
    }

    private void Update()
    {
        // UI beállítások
        if (isTriggered)
        {
            useTextMeshPro.text = String.Format(originalUseText, isDoorOpen ? "close" : "open");
        }
        
        // Ha trigger zónában vagyunk és lenyomjuk az "E" billentyűt
        if (isTriggered && Input.GetKeyDown(KeyCode.E))
        {
            isDoorOpen = !isDoorOpen;
            doorAnimator.SetBool(IsDoorOpened, isDoorOpen);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        isTriggered = true;
        usePanel.SetActive(isTriggered);
        
        // isDoorOpen = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isTriggered = false;
        usePanel.SetActive(isTriggered);
        
        // isDoorOpen = false
    }
}
