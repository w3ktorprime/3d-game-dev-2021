using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectScript : MonoBehaviour
{
    private MeshRenderer meshRenderer;

    [SerializeField] private Material originalMaterial;
    [SerializeField] private Material highlightedMaterial;
    
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        
        EnableHighlight(false);
    }

    public void EnableHighlight(bool enabled)
    {
        if (meshRenderer != null && originalMaterial != null && highlightedMaterial != null)
        {
            meshRenderer.material = enabled ? highlightedMaterial : originalMaterial;
        }
    }

    private void OnMouseOver()
    {
        EnableHighlight(true);
    }
    
    private void OnMouseExit()
    {
        EnableHighlight(false);
    }
}
