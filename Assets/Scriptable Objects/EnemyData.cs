using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "3DGameDev/Enemy Data", order = 1)]
public class EnemyData : ScriptableObject
{
    public enum EnemyType
    {
        Fire = 0,
        Water = 1,
        Air = 2
    }
 
    public EnemyType enemyType;
    public string name;
    public float healthPoint;
    [Min(0)]
    public float minDamage;
    public float maxDamage;
    public float speed;
    public Material material;
}
