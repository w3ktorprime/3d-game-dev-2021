using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EnemyData), true, isFallback = true)]
[CanEditMultipleObjects]
public class EnemyDataEditor : Editor
{
    public string path = "Assets/Resources/enemyData.txt";

    public override void OnInspectorGUI()
    {
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        DrawDefaultInspector();

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
        
        EditorGUILayout.Space(50);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("PATH:", GUILayout.Width(40));
        path = EditorGUILayout.TextField(path);
        EditorGUILayout.EndHorizontal();

        var enemyData = (EnemyData)target;

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("SAVE"))
        {
            Debug.Log("Save: " + enemyData.name);
            SaveToFile(enemyData);
        }
        if (GUILayout.Button("LOAD"))
        {
            Debug.Log("Load");
            LoadFromFile(enemyData);
        }
        EditorGUILayout.EndHorizontal();
    }

    private void SaveToFile(EnemyData enemyData)
    {
        var enemyDataJsonString = JsonUtility.ToJson(enemyData, true);
        try
        {
            var writer = new StreamWriter(path, false);
            writer.Write(enemyDataJsonString);
            writer.Close();
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void LoadFromFile(EnemyData enemyData)
    {
        try
        {
            var reader = new StreamReader(path);
            var enemyDataJsonString = reader.ReadToEnd();
            JsonUtility.FromJsonOverwrite(enemyDataJsonString, enemyData);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }
}
