using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class EnemyController : MonoBehaviour
{
    [Header("Scriptable Object")]
    [SerializeField] private EnemyData _enemyData;

    [SerializeField] private string name;
    public float healthPoint;
    [SerializeField] private float minDamage;
    [SerializeField] private float maxDamage;
    [SerializeField] private float speed;
    [SerializeField] private Material material;
    

    private void Awake()
    {
        name = _enemyData.name;
        healthPoint = _enemyData.healthPoint;
        minDamage = _enemyData.minDamage;
        maxDamage = _enemyData.maxDamage;
        speed = _enemyData.speed;
        material = _enemyData.material;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "Enemy (" + name + ")";
        gameObject.tag = _enemyData.enemyType.ToString();
        gameObject.GetComponent<Renderer>().material = material;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
